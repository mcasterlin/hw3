# ---------------------------------------------------- #
# File: hw3.py
# ---------------------------------------------------- #
# Author(s): Michael Casterlin & Nita Soni, Mcasterlin
# ---------------------------------------------------- #
# Plaftorm:    {Windows 7}
# Environment: Python 2.7.8
# Libaries:    numpy 1.9.0
#              matplotlib 1.4.0
#              scipy 0.14.0-Py2.7
#              Tkinter
# ---------------------------------------------------- #
# Known Issues:
# -Must wait for animation to complete before selecting new Pendulum Type.
# -Simulation of the Double Pendulum is very senstive to changing variable 
#  inputs. This can be seen as the system begins to respond with chaotic 
#  behavior after approximately 7 seconds. We can see on the Theta 1 plot
#  that the behavior can be similar to a sine wave. We speculate that the 
#  first link is becoming dominant and the system is acting as an extended
#  Single Pendulum. One theory is that the time step is too large for the 
#  numerical method computation used; however, we have not tested this out 
#  significantly as the simulation becomes too resource intensive.
#
# Future Iterations:
# -Add n-link functionality
# -Add variable links and masses for addition pendulum link(s)
# -Add variable friction and gravity field
# ---------------------------------------------------- #
"""
#Psuedcode

import libraries: Tkinter, numpy, matplotlib, scipy

specify TITLE_FONT global variable for GUI

Create Pendulum class (inherit from object)
    Initialize parameters and initial conditions
    Unpack dictionary to pendulum variables (method 'properties')
    Create state space based on initial conditions (method 'stateSolve')
    Solve datafield from state equations (method 'equation'):

        dydx[0] = state_omega1
        dydx[1] = -(2*g/Length1)*sin(state_theta1)
    
    Make callable to return datafield

Create Double_Pendulum class (inherit from Pendulum)
    Initialize parameters and initial conditions
    Unpack dictionary to pendulum variables (method 'properties')
    Create state space based on initial conditions (method 'stateSolve')
    Solve datafield from state equations (method 'equation'):

        dydx[0] = state_omega1
        dydx[2] = state_omega2

        cos_delta = cos(state_theta2 - state_theta1)
        sin_delta = sin(state_theta2 - state_theta1)
        denom1 = (M1i + M2i)*L1i*cos_delta*cos_delta
        denom2 = (L2i/L1i)*denom1

        dydx[1] = ( M2i*L1i*state_omega1*state_omega1*sin_delta*cos_delta
                    + M2i*g*sin(state_theta2)*cos_delta
                    + M2i*L2i*state_omega2*state_omega2*sin_delta
                    - (M1i + M2i)*g*sin(state_theta1))
                    /denom1
        dydx[3] = (-M2i*L2i*state_omega2*state_omega2*sin_delta*cos_delta
                   + (M1i + M2i)*g*sin(state_theta1)*cos_delta
                   - (M1i + M2i)*L1i*state_omega1*state_omega1*sin_delta
                   - (M1i + M2i)*g*sin(state_theta2))
                   /denom2

    Make callable to return datafield

Create GUI class (inherit from Tk.Frame)
    Initialize Frame window, make grid space, and plot figure
    Initialize parameter dictionary for passing to Pendulum objects
    Create widgets (method 'createWidgets')
        Create functions for Labels and Sliders
        Create Buttons for Quit and Plot
        Create Sliders for Length, Mass, Angle, Angular Velocity
            (Note: In double pendulum, both links use same values.)
        Create Option Menus for selecting Number of Links and Plot parameters
        Create Entry Boxes for simulation run time and step size
    Create method to convert current widget settings to GUI attributes ('set_ic')
    Create method to assign input attributes to dictionary keys ('packParameters')
    Create method for simulation and plotting
        Call 'set_ic' to get parameters
        Call 'packParameters' to prepare dictionary
        Determine number of links and create respective n-link-pendulum object
        Use pendulum object's callable function to get datafield matrix
        Separate position vectors into cartesian components
        Determine axis limits for time plots
        Embed figure into canvas
        Generate plots according to chosen parameter
        Display and save simulation animation
            (The 'animate' method connects the end points of each link with each other
             and the origin.)

Main
    Create application GUI object
    Title object
    Enter mainloop
    Destroy upon Quit
"""
# ---------------------------------------------------- #

#Import all relevant libraries
import Tkinter as tk
import numpy as np
from numpy import sin, pi, cos
import matplotlib
matplotlib.use('TkAgg')
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import scipy.integrate as integrate

#Set font for GUI
TITLE_FONT = ("Helvetica", 10)

class Pendulum(object):
    
    def __init__(self, parameters, timerange):
        
        #Set initial conditions
        self.g = 9.81
        self.p = parameters
        self.ic = np.zeros([1,0])
        self.t = timerange
        
        #Unpack parameters from dictionary
        self.properties()
        #Solve state equations
        self.stateSolve()

    def __call__(self):
        return self.field
    
    #Unpack parameters from dictionary
    def properties(self):
        self.th1 = self.p['th1']
        self.w1  = self.p['w1']
        self.th2 = self.p['th2']
        self.w2  = self.p['w2']
        self.L1i = self.p['L1i']
        self.L2i = self.p['L2i']
        self.M1i = self.p['M1i']
        self.M2i = self.p['M2i']

    #Solve state equations
    def stateSolve(self):
        # initial state
        state = np.array([self.th1, self.w1])*pi/180

        # integrating ODE
        self.field = integrate.odeint(self.equation, state, self.t)

    #Single pendulum forward dynamic equations
    def equation(self, state, t):
        dydx = np.zeros_like(state)
        dydx[0] = state[1]
        dydx[1] = -(2*self.g/self.L1i)*np.sin(state[0])
        return dydx

class Double_Pendulum(Pendulum):

    def __init__(self, parameters, timerange):
        
        #Set inital conditions
        self.g = 9.81
        self.p = parameters
        self.ic = np.zeros([2,0])
        self.t = timerange
        
        #Unpack parameters from dictionary (inherited)
        self.properties()
        #Solve state equations
        self.stateSolve()

    #Solve state equations
    def stateSolve(self):

        # initial state
        state = np.array([self.th1, self.w1, self.th2, self.w2])*pi/180.

        # integrating ODE
        self.field = integrate.odeint(self.equation, state, self.t)

    #Double pendulum forward dynamic equations
    def equation(self, state, t):
        dydx = np.zeros_like(state)

        dydx[0] = state[1]
        dydx[2] = state[3]

        cos_delta = cos(state[2] - state[0])
        sin_delta = sin(state[2] - state[0])
        denom1 = (self.M1i + self.M2i)*self.L1i*cos_delta*cos_delta
        denom2 = (self.L2i/self.L1i)*denom1

        dydx[1] = ( self.M2i*self.L1i*state[1]*state[1]*sin_delta*cos_delta + self.M2i*self.g*sin(state[2])*cos_delta + self.M2i*self.L2i*state[3]*state[3]*sin_delta - (self.M1i + self.M2i)*self.g*sin(state[0]))/denom1
        dydx[3] = (-self.M2i*self.L2i*state[3]*state[3]*sin_delta*cos_delta + (self.M1i + self.M2i)*self.g*sin(state[0])*cos_delta - (self.M1i + self.M2i)*self.L1i*state[1]*state[1]*sin_delta - (self.M1i + self.M2i)*self.g*sin(state[2]))/denom2
        
        return dydx

class GUI(tk.Frame):

    def __init__(self, master=None):
        tk.Frame.__init__(self, master)

        self.master = master
        self.grid()
        self.createWidgets()
        self.fig = plt.figure()

        #initializing parameter dictionary
        self.inputs = {}
    
    def createWidgets(self):

        #Create widget Label method
        def create_Label(lb_text, position):
            return tk.Label(self,text=lb_text,font=TITLE_FONT).grid(column=position[0],row=position[1])

        #Create slider widget method
        def create_Slider(lb_text, position, span, sl_resolution, default):
            create_Label(lb_text,position)
            slider = tk.Scale(self, from_=span[0], to=span[1], resolution=sl_resolution)
            slider.grid(column=position[0],row=(position[1]+1))
            slider.set(default)
            return slider

        #Create buttons                
        self.quit = tk.Button(self, text="Quit", fg="red", command=self.quit).grid(column=0,row=0)
        self.plot = tk.Button(self, text="Plot", fg="blue",command=self.plot).grid(column=0,row=1)

        #Create sliders       
        self.l1        = create_Slider("Length",(1,0),(0.10,2.5), 0.01, 1.0)
        self.m1        = create_Slider("Mass",(2,0),(0.10,10), 0.1, 1.0)
        self.angle     = create_Slider("Angle",(3,0),(0.00,180.), 1, 100.)
        self.angle_vel = create_Slider("Ang Vel",(4,0),(-5.,5.), 0.1, 0.0)
                
        #Create Drop down menu box
        create_Label("Pendulum Type: ",(6,0))
        self.penvar = tk.StringVar()
        self.penvar.set("Single") # default value
        self.penlist = tk.OptionMenu(self, self.penvar, "Single", "Double").grid(column=7, row=0)

        create_Label("Graph Parameter: ",(6,1))
        self.dbvar = tk.StringVar()
        self.dbvar.set("Theta 1") # default value
        self.dblist = tk.OptionMenu(self, self.dbvar, "Theta 1", "Theta 2", "Omega 1", "Omega 2").grid(column=7, row=1)

        #Create Entry boxes
        self.label_tinit = create_Label("time = ",(8,0))                        
        self.time = tk.StringVar()
        entry = tk.Entry(self,textvariable=self.time).grid(column=9,row=0)
        self.time.set("7.0")
        
        self.label_tinit = create_Label("dt = ",(8,1))
        self.dt_entry = tk.StringVar()
        entry = tk.Entry(self,textvariable=self.dt_entry).grid(column=9,row=1)
        self.dt_entry.set("0.05")

    def set_ic(self):

        #Make time array
        self.t = None
        self.dt = float(self.dt_entry.get())
        self.t = np.arange(0.0, float(self.time.get()), self.dt)

        #Initial mass and length values   
        self.L1i = self.l1.get()
        self.M1i = self.m1.get()
        self.L2i = self.l1.get()
        self.M2i = self.m1.get()
        
        # th is the initial angle (degrees)
        # w  is the initial angular velocity (degrees per second)
        self.th1 = self.angle.get()
        self.w1 = self.angle_vel.get()
        self.th2 = self.angle.get()
        self.w2 = self.angle_vel.get()

    #Pack parameters into dictionary
    def packParameters(self):
        self.inputs['th1'] = self.th1
        self.inputs['w1']  = self.w1
        self.inputs['th2'] = self.th2
        self.inputs['w2']  = self.w2
        self.inputs['M1i'] = self.M1i
        self.inputs['M2i'] = self.M2i
        self.inputs['L1i'] = self.L1i
        self.inputs['L2i'] = self.L2i

    def plot(self):

        #Pack parameters to be used by Pendulum objects
        self.set_ic()
        self.packParameters()

        #Create relevant pendulum objects
        if self.penvar.get() == "Single":
            self.system = Pendulum(self.inputs, self.t)
        elif self.penvar.get() =="Double":
            self.system = Double_Pendulum(self.inputs, self.t)
        else:
            print "Error in pendulum selection."
            print self.penvar

        #Get datafield matrix
        y = self.system.field

        #Break y into component positions
        self.x1 =  self.L1i*sin(y[:,0])
        self.y1 = -self.L1i*cos(y[:,0])
        if self.penvar.get()=="Double":
            self.x2 =  self.x1 +  self.L2i*sin(y[:,2])
            self.y2 =  self.y1 + -self.L2i*cos(y[:,2])
        
        self.fig.clear()

        #Get data boundaries
        t_max =max(self.t)
        if self.dbvar.get() == "Theta 1":   
            th_max=max(y[:,0])
        elif self.dbvar.get() == "Theta 2":
            th_max=max(y[:,2])
        elif self.dbvar.get() == "Omega 1":
            th_max=max(y[:,1])
        else:
            th_max=max(y[:,3])

        #Embed figure into canvas
        canvas = FigureCanvasTkAgg(self.fig, master=self)
        canvas.get_tk_widget().grid(column=0, row= 10, columnspan=10)

        #Create plot space
        ax = self.fig.add_subplot(211, autoscale_on=False, xlim=(-2.5, 2.5), ylim=(-2.5, 2.5))
        ay = self.fig.add_subplot(212, autoscale_on=False, xlim=(0,t_max+1), ylim=(-th_max-1,th_max+1))
        ax.grid()
        ay.grid()

        self.line1, = ax.plot([], [], 'o-', lw=2)
        self.line2, = ax.plot([], [], 'o-', lw=2)

        #Graph time plot
        if self.dbvar.get() =="Theta 1":
            self.curves, = ay.plot(self.t, y[:,0],'o-', lw=2)
        elif self.dbvar.get() == "Theta 2":
            self.curves, = ay.plot(self.t, y[:,1],'*-',lw=2)
        elif self.dbvar.get() == "Omega 1":
            self.curves, = ay.plot(self.t, y[:,2],'^-',lw=2)
        else:
            self.curves, = ay.plot(self.t, y[:,3],'-',lw=2)

        #Animate
        self.time_template = 'time = %.1fs'
        self.time_text = ax.text(0.05, 0.9, '', transform=ax.transAxes)
        self.time_text.set_text('')
        self.ani = animation.FuncAnimation(self.fig, self.animate, np.arange(1, len(y)),
                                      interval=25, blit=False, init_func=self.init, repeat=False)
        
        #Save and show animation
        self.ani.save('pendulum.mp4', fps=30)
        canvas.show()
    
    def init(self):
        return self.line1, self.line2, self.time_text

    def animate(self,i):

        self.time_text.set_text(self.time_template%(i*self.dt))

        #Draw connecting lines to represent links
        self.thisx = [0, self.x1[i]]
        self.thisy = [0, self.y1[i]]
        self.line1.set_data(self.thisx, self.thisy)
   
        if self.penvar.get()=="Double":
            self.thatx = [self.x1[i], self.x2[i]]
            self.thaty = [self.y1[i], self.y2[i]]
            self.line2.set_data(self.thatx, self.thaty)      
            return self.line1, self.line2, self.time_text

        return self.line1, self.time_text

def main():
    root = tk.Tk()
    app = GUI(master=root)
    app.master.title("Pendulum simulation")
    app.mainloop()
    root.destroy()

main()
