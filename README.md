# README #
[single pendulum video](https://drive.google.com/file/d/0B-bww-49YMBcMjlMSEtxbHcyTXc/view?usp=sharing)

[double pendulum video](https://drive.google.com/file/d/0B-bww-49YMBcNk5RVlBmRTZOQm8/view?usp=sharing)

[hw3_screenshot.png](https://bitbucket.org/repo/eno9RB/images/124474834-hw3_screenshot.png)

**Known Issues:**

-Must wait for animation to complete before selecting new Pendulum Type or re-plotting.

-Simulation of the Double Pendulum is very sensitive to changing variable 
 inputs. The best results occur at the default widget settings. It can be 
 seen that the system begins to respond with undesired chaotic 
 behavior after approximately 7 seconds. We have observed on the Theta 1 plot
 that the behavior can sometimes resemble a sine wave. We speculate that the 
 first link is becoming dominant and the system is acting as an extended
 Single Pendulum. One theory is that the time step is too large for the 
 numerical method computation used; however, we have not tested this out 
 significantly as the simulation becomes too resource intensive.

**Future Iterations:**

-Add n-link functionality

-Add variable lengths and masses for additional pendulum link(s)

-Add variable friction and gravity field